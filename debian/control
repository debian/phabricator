Source: phabricator
Section: web
Priority: optional
Maintainer: Sylvestre Ledru <sylvestre@debian.org>
Uploaders:
 Christoph Biedl <debian.axhn@manchmal.in-ulm.de>,
Build-Depends: debhelper-compat (= 13), po-debconf
Standards-Version: 4.1.5
Homepage: http://phabricator.org/
Vcs-Browser: https://salsa.debian.org/debian/phabricator
Vcs-Git: https://salsa.debian.org/debian/phabricator.git

Package: libphutil
Architecture: all
Depends: php-xml, ${misc:Depends}
Recommends: php-cli
Description: Shared library for Arcanist and Phabricator
 libphutil (pronounced as "lib-futile", like the English word futile)
 is principally the shared library for Arcanist and Phabricator,
 but is suitable for inclusion in other projects.
 In particular, some of the classes provided in this library improve the state
 of common operations in PHP, like executing system commands.

Package: arcanist
Architecture: all
Depends: libphutil (= ${source:Version}), php-cli, php-curl, ${misc:Depends},
 php-xml
Conflicts: arc
Suggests: python
Description: Command line interface for Phabricator (review platform)
 Arcanist is the command-line tool for Phabricator. It allows you to interact
 with Phabricator installs to send code for review, download patches, transfer
 files, view status, make API calls, and various other things.

# Package: phabricator
# Architecture: all
# Depends: ${misc:Depends},
#          arcanist,
#          ca-certificates,
#          crudini,
#          dbconfig-common (>= 1.8.8),
#          fonts-font-awesome,
#          imagemagick,
#          jq,
#          libapache2-mod-php | php-fpm,
#          libjs-d3,
#          libphutil (= ${source:Version}),
#          php-apcu,
#          php-cli,
#          php-curl,
#          php-gd,
#          php-mailparse,
#          php-mbstring,
#          php-mysql | php-mysqli | php-mysqlnd,
#          php-zip,
#          php,
#          po-debconf,
#          python3-pygments,
#          python3-pkg-resources,
#          ucf,
# Recommends:
#    apache2 (>= 2.2.7) | lighttpd | nginx | httpd,
#    mariadb-server | default-mysql-server | virtual-mysql-server,
# Suggests: python, npm
# Description: Software engineering platform
#  Phabricator is an open source collection of web applications which make it
#  easier to write, review, and share source code.
#  .
#  Features:
#   * Review Code: Review others' code with Differential
#   * Track Bugs: You can keep track of all the defects,
#     and problems in your awful software with Maniphest.
#   * Browse Source: You can browse source code on the web with Diffusion
#   * Wiki: You can write things down in Phriction
#   * ... and others
